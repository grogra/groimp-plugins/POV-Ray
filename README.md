# POV-Ray plugin 

POV-Ray plugin enables import and export of the scene to a POV file. The export embeds the POV-Ray ray-tracer so that it can directly render in the 3D view.

It includes support of the file type :
- ".pov" 

