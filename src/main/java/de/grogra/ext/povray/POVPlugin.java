
/*
 * Copyright (C) 2002 - 2007 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.ext.povray;

import de.grogra.pf.registry.*;

public class POVPlugin extends Plugin
{

	@Override
	public void startup ()
	{
		super.startup ();

		
		if (!checkConfigure ())
		{
			return;
		}
		Item opts = getPluginDescriptor ().getRegistry ().getItem ("/renderers/3d/povray");
		if (opts == null)
		{
			return;
		}
		if (System.getProperty ("os.name").toLowerCase ().indexOf ("win") >= 0)
		{
			Option o = Option.get (opts, "stdout");
			if (o != null)
			{
				o.setOptionValue (Boolean.FALSE);
			}
			o = Option.get (opts, "command");
			if (o != null)
			{
				o.setOptionValue (o.getObject () + " /exit /nr");
			}
		}
	}

}
